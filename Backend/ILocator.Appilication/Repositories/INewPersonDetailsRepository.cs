﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ILocator.Appilication.Models;
using ILocator.Appilication.ViewModels;
using ILocator.Domain.NoSqlEntities;

namespace ILocator.Appilication.Repositories
{
    public interface INewPersonDetailsRepository
    {
        Task InsertPersonDetailsAsync(NewPersonsDetails person);
    }
}
