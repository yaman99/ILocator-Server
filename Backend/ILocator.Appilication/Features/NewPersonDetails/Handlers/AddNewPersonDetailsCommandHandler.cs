﻿using AutoMapper;
using ILocator.Appilication.Features.NewPersonDetails.Commands;
using ILocator.Appilication.Models;
using ILocator.Appilication.Repositories;
using ILocator.Domain.NoSqlEntities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ILocator.Appilication.Features.NewPersonDetails.Handlers
{
    public class AddNewPersonDetailsCommandHandler : IRequestHandler<AddNewPersonDetailsCommand, Result>
    {
        private readonly INewPersonDetailsRepository _newPersonRepo;
        private readonly IMapper _mapper;

        public AddNewPersonDetailsCommandHandler(INewPersonDetailsRepository newPersonRepo, IMapper mapper)
        {
            _newPersonRepo = newPersonRepo;
            _mapper = mapper;
        }

        public async Task<Result> Handle(AddNewPersonDetailsCommand request, CancellationToken cancellationToken)
        {
            var data = _mapper.Map<NewPersonsDetails>(request);
            await _newPersonRepo.InsertPersonDetailsAsync(data);
            return Result.Success(data);
        }
    }
}
