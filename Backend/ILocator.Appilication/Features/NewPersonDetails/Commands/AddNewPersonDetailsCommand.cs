﻿using ILocator.Appilication.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Appilication.Features.NewPersonDetails.Commands
{
    public class AddNewPersonDetailsCommand : IRequest<Result>
    {
        public string FirstName { get; set; }
    }
}
