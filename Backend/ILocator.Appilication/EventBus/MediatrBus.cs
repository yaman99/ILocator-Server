﻿
using ILocator.SharedKernel.Bus;
using ILocator.SharedKernel.Events;
using MediatR;
using System;
using System.Threading.Tasks;

namespace ILocator.Application.EventBus
{
    public class MediatrBus : IBus
    {
        private readonly IMediator _mediator;

        public MediatrBus(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<TResponse> ExecuteAsync<T, TResponse>(T command) where T : IRequest<TResponse>
        {
           return  await _mediator.Send(command);
        }

      

        public async Task RaiseEvent<T>(T @event) where T : Event
        {
            await _mediator.Publish(@event);
        }

        /*private INotification GetNotificationCorrespondingToDomainEvent(Event domainEvent)
        {
            return (INotification)Activator.CreateInstance(
                typeof(DomainEventNotification<>).MakeGenericType(domainEvent.GetType()), domainEvent);
        }*/
    }
}
