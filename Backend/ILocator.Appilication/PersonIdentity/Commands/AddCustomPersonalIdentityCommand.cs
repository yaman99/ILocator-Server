﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ILocator.Domain.Entities;
using System.Text.RegularExpressions;
using ILocator.Appilication.Interfaces;
using ILocator.Appilication.Models;

namespace ILocator.Appilication.PersonIdentityx.Commands
{
    public class AddCustomPersonalIdentityCommand : IRequest<Result>
    {
    }

    public class AddPersonalIdentityCommandHandler : IRequestHandler<AddCustomPersonalIdentityCommand, Result>
    {
        private readonly IMapper _mapper;
        private readonly IAppilicationDbContext _context;

        public AddPersonalIdentityCommandHandler(IAppilicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Result> Handle(AddCustomPersonalIdentityCommand request, CancellationToken cancellationToken)
        {
            var startPosition = 1;

            Regex khaaWithNumber = new(@"([\u062E\u0640]+)(\d+)");
            Regex lettersWithNumbers = new(@"(?<char>[\u0621-\u064A\s]+)(?<num>\d+)");

            while (startPosition < 20)
            {
                var data = await _context.PersonIdentity.Skip(startPosition).Take(10).ToListAsync();
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        //if(item.Registration.Any(x=> char.IsDigit(x)) && item.RegistrationNo == null)
                        //{
                        //    continue;
                        //}

                        var itemString = item.Registration
                            .Split(",").FirstOrDefault()
                            .Replace(" خ ", " ")
                            .Replace("/", "")
                            .Replace("-", "")
                            .Replace("_", "")
                            .Replace(" ج ", "");


                        if (khaaWithNumber.IsMatch(itemString))
                        {
                            itemString = itemString.Replace("خ", "").Replace("ـ", "");
                        }
                        if (lettersWithNumbers.IsMatch(itemString))
                        {
                            Match result = lettersWithNumbers.Match(itemString);
                            string alphaPart = result.Groups["char"].Value;
                            string numberPart = result.Groups["num"].Value;

                            item.CustomReg = alphaPart;
                            item.CustomRegNo = numberPart;
                        }
                        else
                        {
                            //personModel.Add(PrepareModel(item.Registration, item.RegistrationNo, itemString, item.RegistrationNo, false));
                            item.CustomReg = itemString;
                            item.CustomRegNo = item.RegistrationNo;
                        }
                        //personModel.Add(PrepareModel(itemString, item.RegistrationNo));

                    }
                    var mappedData = _mapper.Map<List<PersonIdentity>, List<CustomPersonIdentity>>(data);
                    _context.CustomPersonIdentity.BulkInsert(mappedData);
                    startPosition += 10;

                }
                else
                {
                    break;
                }

            }
            return Result.Success();
        }
    }
}
