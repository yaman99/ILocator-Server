﻿using AutoMapper;
using ILocator.Appilication.Features.NewPersonDetails.Commands;
using ILocator.Domain.Entities;
using ILocator.Domain.NoSqlEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Appilication.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PersonIdentity, CustomPersonIdentity>();
            CreateMap<AddNewPersonDetailsCommand, NewPersonsDetails>()
                .ConstructUsing( x => new NewPersonsDetails(Guid.NewGuid()));
        }
    }
}
