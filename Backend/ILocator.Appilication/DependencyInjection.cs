﻿using FluentValidation;
using ILocator.Appilication.Behaviours;
using ILocator.Appilication.Repositories;
using ILocator.Application.Common.Behaviours;
using ILocator.Application.EventBus;
using ILocator.SharedKernel.Bus;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Appilication
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddAppilication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<IBus, MediatrBus>();
            
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            return services;
        }
    }
}
