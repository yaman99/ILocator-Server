﻿using ILocator.Appilication.Models;
using ILocator.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Appilication.Interfaces.Repos
{
    public interface IPersonIdentityRepository
    {
        Task<List<ILocator.Domain.Entities.PersonIdentity>> GetPersonsByRange(int startPos);
        Task<List<ILocator.Domain.Entities.PersonIdentity>> GetLastPersons();
        //void UpdateProcessedData(List<PersonIdentity> personIdentity);
        Task UpdateProcessedData(List<CustomPersonIdentity> personIdentity);
    }
}
