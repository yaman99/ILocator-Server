﻿using ILocator.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ILocator.Appilication.Interfaces
{
    public interface IAppilicationDbContext
    {
        DbSet<ILocator.Domain.Entities.PersonIdentity> PersonIdentity { get; set; }
        DbSet<CustomPersonIdentity> CustomPersonIdentity { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
