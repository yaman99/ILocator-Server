﻿using Autofac;
using ILocator.Appilication.Interfaces;
using ILocator.Appilication.Repositories;
using ILocator.Infrastructure.Repositories;
using ILocator.SharedKernel;
using ILocator.SharedKernel.Mongo;
using ILocator.SharedKernel.Types;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using System;

namespace ILocator.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppilicationDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("IdentityConnection")));

            services.AddScoped<IAppilicationDbContext>(provider => provider.GetService<AppilicationDbContext>());
            //services.AddTransient<INewPersonDetailsRepository, NewPersonDetailsRepository>();
            //services.AddTransient<IPersonIdentityRepository, PersonIdentityRepository>();

            return services;
        }

        public static void AddMongo(this ContainerBuilder builder)
        {
            builder.Register(context =>
            {
                var configuration = context.Resolve<IConfiguration>();
                var options = configuration.GetOptions<MongoDbOptions>("mongo");

                return options;
            }).SingleInstance();

            builder.Register(context =>
            {
                var options = context.Resolve<MongoDbOptions>();

                return new MongoClient(options.ConnectionString);
            }).SingleInstance();

            builder.Register(context =>
            {
                var options = context.Resolve<MongoDbOptions>();
                var client = context.Resolve<MongoClient>();
                return client.GetDatabase(options.Database);

            }).InstancePerLifetimeScope();

        }

        public static void AddMongoRepository<TEntity, T>(this ContainerBuilder builder, string collectionName)
            where TEntity : IIdentifiable<T>
            => builder.Register(ctx => new MongoRepository<TEntity, T>(ctx.Resolve<IMongoDatabase>(), collectionName))
                .As<IMongoRepository<TEntity, T>>()
                .InstancePerLifetimeScope();
    }
}
