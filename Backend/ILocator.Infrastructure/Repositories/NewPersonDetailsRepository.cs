﻿using ILocator.Appilication.Models;
using ILocator.Appilication.Repositories;
using ILocator.Appilication.ViewModels;
using ILocator.Domain.NoSqlEntities;
using ILocator.SharedKernel.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Infrastructure.Repositories
{
    public class NewPersonDetailsRepository : INewPersonDetailsRepository
    {
        private readonly IMongoRepository<NewPersonsDetails , Guid> _repository;

        public NewPersonDetailsRepository(IMongoRepository<NewPersonsDetails, Guid> mongoRepository)
        {
            _repository = mongoRepository;
        }

        public async Task InsertPersonDetailsAsync(NewPersonsDetails person)
            => await _repository.AddAsync(person);
    }
}
