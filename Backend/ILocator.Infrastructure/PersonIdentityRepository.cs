﻿using ILocator.Appilication.Interfaces;
using ILocator.Appilication.Interfaces.Repos;
using ILocator.Appilication.Models;
using ILocator.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Infrastructure
{
    public class PersonIdentityRepository : IPersonIdentityRepository
    {
        private readonly AppilicationDbContext _context;

        public PersonIdentityRepository(AppilicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<PersonIdentity>> GetPersonsByRange(int startPos)
        {
            return await _context.PersonIdentity.Where(x => x.Registration == "جوبر").Skip(startPos).Take(50).ToListAsync();
            //return await _context.PersonIdentity.Where(x => x.Registration == "جوبر").Take(50).ToListAsync();

        }

        //public void UpdateProcessedData(List<PersonIdentity> personIdentity)
        //{
        //    _context.PersonIdentity.BulkUpdateAsync(personIdentity).GetAwaiter();
        //}

        public async Task UpdateProcessedData(List<CustomPersonIdentity> personIdentity)
        {
            _context.CustomPersonIdentity.AddRange(personIdentity);
            await _context.BulkSaveChangesAsync();
        }

        public async Task<List<PersonIdentity>> GetLastPersons()
        {
            return await _context.PersonIdentity.TakeLast(50).ToListAsync();
        }
    }
}
