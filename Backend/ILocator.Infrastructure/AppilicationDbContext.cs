﻿using ILocator.Appilication.Interfaces;
using ILocator.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ILocator.Infrastructure
{
    public class AppilicationDbContext : DbContext , IAppilicationDbContext
    {
        public AppilicationDbContext(DbContextOptions<AppilicationDbContext> options) : base(options)
        {
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
           
            return await base.SaveChangesAsync(cancellationToken);

        }


        public DbSet<PersonIdentity> PersonIdentity {get; set;}
        public DbSet<CustomPersonIdentity> CustomPersonIdentity {get; set;}
    }
}
