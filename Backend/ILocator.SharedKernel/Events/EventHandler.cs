﻿using ILocator.SharedKernel.Notifications;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ILocator.SharedKernel.Events
{
    //Event listener that listen for any domain error notification and take corresponding action
    public class EventHandler : INotificationHandler<DomainErrorNotification>
    {
        public Task Handle(DomainErrorNotification notification, CancellationToken cancellationToken)
        {
            //TODO : Add logging provider to log this error in db

            throw new InvalidOperationException();
        }
    }
}
