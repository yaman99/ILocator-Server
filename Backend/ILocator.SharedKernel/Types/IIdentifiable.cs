namespace ILocator.SharedKernel.Types
{
    public interface IIdentifiable<T> 
    {
         T Id { get; }
    }
}