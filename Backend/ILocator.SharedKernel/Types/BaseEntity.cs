using ILocator.SharedKernel.Events;
using System;
using System.Collections.Generic;

namespace ILocator.SharedKernel.Types
{
    public abstract class BaseEntity<T> : IIdentifiable<T>
    {
        public T Id { get; protected set; }
        public DateTime CreatedDate { get; protected set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; protected set; }
        public string LastModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public BaseEntity(T id)
        {
            Id = id;
            CreatedDate = DateTime.UtcNow;
            SetUpdatedDate();
        }

        protected virtual void SetUpdatedDate()
            => UpdatedDate = DateTime.UtcNow;

       
    }
}