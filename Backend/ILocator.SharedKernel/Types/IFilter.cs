using MediatR;
using System.Collections.Generic;

namespace ILocator.SharedKernel.Types
{
    public interface IFilter<TResult, in TQuery> where TQuery : IRequest<TResult>
    {
        IEnumerable<TResult> Filter(IEnumerable<TResult> values, TQuery query);
    }
}