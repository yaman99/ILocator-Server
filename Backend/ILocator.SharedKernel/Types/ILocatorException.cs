using System;

namespace ILocator.SharedKernel.Types
{
    public class ILocatorException : Exception
    {
        public string Code { get; }

        public ILocatorException()
        {
        }

        public ILocatorException(string code)
        {
            Code = code;
        }

        public ILocatorException(string message, params object[] args) 
            : this(string.Empty, message, args)
        {
        }

        public ILocatorException(string code, string message, params object[] args) 
            : this(null, code, message, args)
        {
        }

        public ILocatorException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public ILocatorException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }        
    }
}