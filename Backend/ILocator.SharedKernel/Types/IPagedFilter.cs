
using MediatR;
using System.Collections.Generic;

namespace ILocator.SharedKernel.Types
{
    public interface IPagedFilter<TResult, in TQuery> where TQuery : IRequest<TResult>
    {
        PagedResult<TResult> Filter(IEnumerable<TResult> values, TQuery query);
    }
}