﻿namespace ILocator.SharedKernel.FrontEnd
{
    public abstract class NgxsAction<T> where T : IViewModel
    {
        public string Type { get; }
        public T Payload { get; }

        public NgxsAction(string type, T payload)
        {
            Type = type;
            Payload = payload;
        }
    }
}
