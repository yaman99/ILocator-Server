﻿using System.Threading.Tasks;

namespace ILocator.SharedKernel.FrontEnd
{
    public  interface IWebsocketEmitter
    {
        Task Emit<T>(NgxsAction<T> action) where T : IViewModel;
    }
}
