﻿using ILocator.SharedKernel.Events;
using MediatR;
using System.Threading.Tasks;

namespace ILocator.SharedKernel.Bus
{
    public interface IBus
    {
     
        Task<TResponse> ExecuteAsync<T, TResponse>(T command) where T : IRequest<TResponse>;
        Task RaiseEvent<T>(T @event) where T : Event;
    }
}
