﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;
namespace ILocator.SharedKernel.JilSerializer
{
    public class JilOutFormatter : TextOutputFormatter
    {
        public JilOutFormatter()
        {
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);

            SupportedMediaTypes.Add(ContentTypeValues.AppJson);
            SupportedMediaTypes.Add(ContentTypeValues.TxtJson);
        }

        public override bool CanWriteResult(OutputFormatterCanWriteContext context)

        {
            if(context == null)throw new ArgumentNullException(nameof(context));
            return context.ContentType.ToString() == "application/json" || context.ContentType.ToString() == "text/json";
        }
        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding encoding)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            var response = context.HttpContext.Response;

            using (var writer = context.WriterFactory(response.Body, encoding))
            {
                JilHelper.Instance.Serialize(writer, context.Object);
                await writer.FlushAsync();
            }
        }
    }
}
