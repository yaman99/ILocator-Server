﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ILocator.SharedKernel.JilSerializer
{
    public sealed class JilSettings
    {
        public static readonly JsonSerializerSettings JavascriptSerializer;
        static JilSettings()
        {
            JavascriptSerializer = CreateJavascriptSerializer();
        }


        private static JsonSerializerSettings CreateJavascriptSerializer()
        {
            return new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error,
                DateParseHandling = DateParseHandling.DateTimeOffset,
                DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DefaultValueHandling = DefaultValueHandling.Include,
                FloatFormatHandling = FloatFormatHandling.DefaultValue,
                NullValueHandling = NullValueHandling.Include,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Error,
                StringEscapeHandling = StringEscapeHandling.EscapeNonAscii,
                TypeNameHandling = TypeNameHandling.None,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }
    }
}
