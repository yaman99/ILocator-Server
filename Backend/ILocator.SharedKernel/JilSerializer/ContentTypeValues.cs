﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;

namespace ILocator.SharedKernel.JilSerializer
{
    internal class ContentTypeValues
    {
        public static readonly MediaTypeHeaderValue AppJson = MediaTypeHeaderValue.Parse("application/json").CopyAsReadOnly();

        public static readonly MediaTypeHeaderValue TxtJson = MediaTypeHeaderValue.Parse("text/json").CopyAsReadOnly();
    }
}
