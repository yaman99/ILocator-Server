﻿using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.SharedKernel.JilSerializer
{
    public class JilInputFormatter : TextInputFormatter
    {
        public JilInputFormatter()
        {
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);

            SupportedMediaTypes.Add(ContentTypeValues.AppJson);
            SupportedMediaTypes.Add(ContentTypeValues.TxtJson);
        }
        public override bool CanRead(InputFormatterContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var contentType = context.HttpContext.Request.ContentType;
            return contentType == null || contentType.Contains("application/json") || contentType.Contains("text/json");
        }
        public override Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context, Encoding encoding)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var request = context.HttpContext.Request; 
            if (request.ContentLength == 0)
            {
                return InputFormatterResult
                    .SuccessAsync(context.ModelType.GetTypeInfo().IsValueType ?
                    Activator.CreateInstance(context.ModelType) : null);
            }

            var req = context.HttpContext.Request;

            req.Body.Position = 0;

            using (var reader = new StreamReader(req.Body, true))
            {
                var model = JilHelper.Instance.Deserialize(reader, context.ModelType);
                reader.Dispose();
                return InputFormatterResult.SuccessAsync(model);
            }
        }
    }
}
