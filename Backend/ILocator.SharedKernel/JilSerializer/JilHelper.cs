﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using Jil;
using System.IO;

namespace ILocator.SharedKernel.JilSerializer
{
    public class JilHelper
    {
        private Options _options;

        private JilHelper()
        {
            _options = new Options(excludeNulls: true, includeInherited: true,
                dateFormat: DateTimeFormat.MillisecondsSinceUnixEpoch,
                serializationNameFormat: SerializationNameFormat.CamelCase);
        }

        public static readonly JilHelper Instance = new JilHelper();

        public void Serialize(TextWriter writer, object data)
        {
            JSON.Serialize(data, writer, _options);
        }
        public object Deserialize(TextReader reader, Type modelType)
        {
           return JSON.Deserialize( reader,modelType, _options);
        }
    }
}
