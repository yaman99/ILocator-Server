﻿using ILocator.SharedKernel.Events;

namespace ILocator.SharedKernel.Notifications
{
    //domain notification event
    public class DomainErrorNotification : Event
    {
        public DomainErrorNotification(string msg)
        {
            Message = msg;
        }

        public string Message { get; }
    }
}
