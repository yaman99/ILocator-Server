﻿using System.Threading.Tasks;

namespace ILocator.SharedKernel.Initializers
{
    public interface IInitializer
    {
        Task InitializeAsync();
    }
}
