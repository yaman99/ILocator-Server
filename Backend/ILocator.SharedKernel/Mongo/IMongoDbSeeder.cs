using System.Threading.Tasks;

namespace ILocator.SharedKernel.Mongo
{
    public interface IMongoDbSeeder
    {
        Task SeedAsync();
    }
}