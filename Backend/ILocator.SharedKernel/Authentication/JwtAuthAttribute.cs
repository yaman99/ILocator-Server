using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace ILocator.SharedKernel.Authentication
{
    public class JwtAuthAttribute : AuthAttribute
    {
        public JwtAuthAttribute(string policy = "") : base(JwtBearerDefaults.AuthenticationScheme, policy)
        {
        }
    }
}