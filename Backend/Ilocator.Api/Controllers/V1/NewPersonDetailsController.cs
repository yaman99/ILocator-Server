﻿using ILocator.Appilication.Features.NewPersonDetails.Commands;
using ILocator.Appilication.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ILocator.Api.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewPersonDetailsController : ILocatorController
    {
        [HttpPost]
        public async Task<IActionResult> InsertNewPersonDetailsAsync(AddNewPersonDetailsCommand command)
        {
            var result = await Bus.ExecuteAsync<AddNewPersonDetailsCommand, Result>(command);

            if (result.Succeeded)
                return Ok(result.Data);
            return BadRequest(result.Errors);

        }
    }
}
