﻿using ILocator.Appilication.Models;
using ILocator.Appilication.PersonIdentityx.Commands;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ILocator.Api.Controllers.V1
{
    [ApiController]
    [Route("[controller]")]
    public class PersonIdentityController : ILocatorController
    {

        public PersonIdentityController()
        {
        }

        [HttpPost("Update-Persons")]
        public async Task<IActionResult> UpdatePersons()
        {
            await Bus.ExecuteAsync<AddCustomPersonalIdentityCommand, Result>(new AddCustomPersonalIdentityCommand());
            return Ok();
        }
    }
}
