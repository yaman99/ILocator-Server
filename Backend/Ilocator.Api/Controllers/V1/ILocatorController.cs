﻿using ILocator.SharedKernel.Bus;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace ILocator.Api.Controllers.V1
{
    public abstract class ILocatorController : ControllerBase
    {
        private IBus _bus;
        protected IBus Bus => _bus ??= HttpContext.RequestServices.GetService(typeof(IBus)) as IBus;
    }
}
