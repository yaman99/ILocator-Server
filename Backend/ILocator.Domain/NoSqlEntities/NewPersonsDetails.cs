﻿using ILocator.SharedKernel.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILocator.Domain.NoSqlEntities
{
    public class NewPersonsDetails : BaseEntity<Guid>
    {
        public string FirstName { get; set; }
        public NewPersonsDetails(Guid id) : base(id)
        {
        }
    }
}
